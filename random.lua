--encoding=utf-8

local setmetatable = setmetatable
local error = error
local time = os.time
local ceil = math.ceil
local floor = math.floor

module(...)

local mt = {__index = _M}

function new(self, value)
    if value then
        return setmetatable({seed = value}, mt)
    else
    	return setmetatable({seed = time()}, mt)
    end
end

local function round(num) 
    if num >= 0 then return floor(num+.5) 
    else return ceil(num-.5) end
end

local function gen(self)
    self.seed = (self.seed * 16807) % 2147483647
    return self.seed
end

function next_double(self)
    return (gen(self) / 2147483647)
end

function next_int(self, n)
    if not n then
        return gen(self)
	else
        local min = 0
        local max = n + .4999
        return round(min + ((max - min) * next_double(self)))
    end
end

function next_range(self, n, m)
    if n and m then
        local min = n
        local max = m + .4999
        return round(min + ((max - min) * next_double(self)))
    else
        return gen(self)
    end
end

function next_double_range(self, min, max)
    return min + ((max - min) * next_double(self))
end

local class_mt = {
    __newindex = function(t, k, v)
        error('attempt to write to undeclare variable "' .. k .. '"')
    end
}

setmetatable(_M, class_mt)
