local random = require('random')
local seed = os.time()
local r1, r2 = random:new(seed), random:new(seed)
local result1, result2 = {},{}

local max_rand_int = 1000000

for i = 1, max_rand_int do 
    table.insert(result1, 0)
    table.insert(result2, 0)
end

result1[0], result2[0] = 0,0

math.randomseed(os.time())

function t1()
    for i = 1, max_rand_int do
        local a,b = math.random(max_rand_int), math.random(max_rand_int)
        result1[a] = result1[a] + 1
    end
end

function t2()
    for i = 1, max_rand_int do
        local a,b = r1:next_int(max_rand_int), r2:next_int(max_rand_int)
        if a ~= b then
            print('false')
        else
            result2[a] = result2[a] + 1
        end
    end
end

function g1()
    local count = 0
    for i = 0, max_rand_int do
        if result1[i] > 1 then
            count = count + 1
            --print('result:' .. i .. ":" .. result[i])
        end
    end
    print('math lib test count is ' .. count)
end

function g2()
    local count = 0
    for i = 0, max_rand_int do
        if result2[i] > 1 then
            count = count + 1
            --print('result:' .. i .. ":" .. result[i])
        end
    end
    print('new random test count is ' .. count)
end

t1()
t2()
g1()
g2()
